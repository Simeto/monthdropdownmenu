/**
 * Created by stanimirov on 2/13/2015.
 */
var SHOW_THE_SUGGEST_WINDOW = (function($){
    'use strict';
    var FEATURE_TESTS = {
            addEventListener: !!window.addEventListener
        },
        VARIABLE_CONTAINER = {
            getTheInputField: $('#month-input-field'),
            getTheSuggestionWindow: $('#suggestionWindow'),
            getTheDropDownButton: $('#monthsDropDownButton'),
            getTheSubmitButton: $('#the-submit-button'),
            getItemStoredAttributes: []

        },
        FUNCTION_CONTAINER = {
            showSuggestWindow: function(  ){
                $(VARIABLE_CONTAINER.getTheSuggestionWindow).removeClass('hideWindow');
            },
            hideTheSuggestWindow: function(){
               $(VARIABLE_CONTAINER.getTheSuggestionWindow).addClass('hideWindow');

            }
        };

    /******Show/hide_the_suggestion_window******/

        if(FEATURE_TESTS.addEventListener){
            VARIABLE_CONTAINER.getTheInputField.keydown(function( event ){
                event = event || window.event;
                /***Down Keyboard Arrow***/
                if(event.keyCode != 40){
                    $(VARIABLE_CONTAINER.getTheSuggestionWindow).removeClass( 'hideWindow' );
                    $(VARIABLE_CONTAINER.getTheInputField).focusout(FUNCTION_CONTAINER.hideTheSuggestWindow);
                }
                /***Down Keyboard Arrow***/
                if(event.keyCode == 40){
                    $(VARIABLE_CONTAINER.getTheSuggestionWindow).removeClass(' hideWindow ');
                    $(VARIABLE_CONTAINER.getTheInputField).focusout(FUNCTION_CONTAINER.showSuggestWindow);
                    $( ".suggestion_window_a:first" ).addClass(' aElementsDecoration ').focus();
                }
                /***38- Up; 39- Right keyboard arrows***/
                if( event.keyCode == 38 || event.keyCode == 39  ){
                    FUNCTION_CONTAINER.hideTheSuggestWindow();
                }
            });

            /******Hide suggestion window when the document , month or submit button is clicked******/
            VARIABLE_CONTAINER.getTheDropDownButton.click(FUNCTION_CONTAINER.hideTheSuggestWindow);
            VARIABLE_CONTAINER.getTheSubmitButton.click(FUNCTION_CONTAINER.hideTheSuggestWindow);
            $(document).click(FUNCTION_CONTAINER.hideTheSuggestWindow);
        }

    /*********Sending_Data_for_Tests----------->>>*/

    var sendDataForTests = {
        eListener: FEATURE_TESTS.addEventListener,
        inputFieldCapture: VARIABLE_CONTAINER.getTheInputField,
        suggestionWindowCapture: VARIABLE_CONTAINER.getTheSuggestionWindow
    };
    /*<<<---------Sending_Data_for_Tests***********/
    return sendDataForTests;
})(jQuery);