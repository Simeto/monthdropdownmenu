/**
 * Created by stanimirov on 2/13/2015.
 */
var AUTOCOMPLETE_FOR_MONTHS = (function($){
    'use strict';
    var FEATURE_TESTS = {
            addEventListener: !!window.addEventListener
        },
        VARIABLE_HOLDER = {
            getTheInputField: $('#month-input-field'),
            getTheSuggestionWindow: $('#suggestionWindow'),   /*Used to send element for tests*/
            getTheAutocompletePanel: $('#autocomplete-panel'),
            suggestionDelayTime: 400
        },
        ARRAY_HOLDER = {
            monthsArray: ['January','February','March','April','May','June','July','August','September','October','November','December'
            ]
        },
        FUNCTION_HOLDER = {
            arrayToLowerCase: $.map(ARRAY_HOLDER.monthsArray, function (item, index) {
                return  item.toLowerCase().split();
            }),
            monthsAutocompleteIteration:  function (){
                if(FEATURE_TESTS.addEventListener){
                    if($(VARIABLE_HOLDER.getTheInputField).keyup(function( event ){
                            event = event || window.event;
                            /***Down Keyboard Arrow***/
                            if(event.keyCode != 40) {
                                VARIABLE_HOLDER.getTheAutocompletePanel.html('');
                                if ($(VARIABLE_HOLDER.getTheInputField).val()) {
                                    var inputValToLowerCase = $(VARIABLE_HOLDER.getTheInputField).val().toLowerCase();
                                    var i;
                                    var len;
                                    var pattern = new RegExp("\\b(" + inputValToLowerCase + ")");
                                    for (i = 0, len = FUNCTION_HOLDER.arrayToLowerCase.length; i < len; i += 1) {
                                        if (pattern.exec(FUNCTION_HOLDER.arrayToLowerCase[i])) {
                                            VARIABLE_HOLDER.getTheAutocompletePanel.append('<li class="suggestion_window_li" >' + '<a class="suggestion_window_a" href="#">' + FUNCTION_HOLDER.arrayToLowerCase[i] + '</a>' + '</li>');
                                        }
                                    }
                                }
                            }
                        })){
                    }
                }
            },
            suggestionsTimeout: function(){
                setTimeout(function(){
                    FUNCTION_HOLDER.monthsAutocompleteIteration();
                },VARIABLE_HOLDER.suggestionDelayTime);
            }
        };
    FUNCTION_HOLDER.suggestionsTimeout();

    /*********Sending_Data_for_Tests----------->>>*/
    var sendForTesting = {
        eListener: FEATURE_TESTS.addEventListener,
        inputFieldCapture: VARIABLE_HOLDER.getTheInputField,
        suggestionWindowCapture:  VARIABLE_HOLDER.getTheSuggestionWindow,
        autocompletePannelCapture: VARIABLE_HOLDER.getTheAutocompletePanel,
        suggestionDelayTimeCapture: VARIABLE_HOLDER.suggestionDelayTime,
        monthsArrayCapture: ARRAY_HOLDER.monthsArray,
        arrayToLowercaseCapture: FUNCTION_HOLDER.arrayToLowerCase,
        monthsAutocompleteIterationCapture: FUNCTION_HOLDER.monthsAutocompleteIteration,
        suggestionsTimeoutCapture: FUNCTION_HOLDER.suggestionsTimeout
    };
    return sendForTesting;
    /*<<<---------Sending_Data_for_Tests***********/
})(jQuery);