/**
 * Created by stanimirov on 3/30/2015.
 */

var CUSTOM_EVENT = (function($){
    'use strict';

    var theBody = document.body;
    theBody.addEventListener("submit", sendNotificationParameters, false);

    // new message: raise newMessage event
    function sendNotificationParameters(e) {
        e = e || window.event;
        var event = new CustomEvent("notify", {
            detail: {
                /* if not passed, sets default global position to 'top right' */
                element: $('#month-input-field'),
                /* default message is an empty message */
                message: 'Please type some month !',
                /* default type is 'success' */
                type: 'warn'
            }
        });
        e.currentTarget.dispatchEvent(event);
    }
})(jQuery);