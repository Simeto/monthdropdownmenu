/**
 * Created by stanimirov on 2/13/2015.
 */
var ADD_MONTH_FROM_DROP_DOWN_LIST = (function($){
        'use strict';

        var FEATURE_TESTS = {
                addEventListener: !!window.addEventListener

            },
            VARIABLE_CONTAINER = {
                getAElements: $('a'),
                getTheInputField: $('#month-input-field')
            };
        if(FEATURE_TESTS.addEventListener){
            $(VARIABLE_CONTAINER.getAElements).click(function(){
                VARIABLE_CONTAINER.getTheInputField.val($(this).text());
                VARIABLE_CONTAINER.getTheInputField.focus();
            });
        }
        /*********Sending_Data_for_Tests----------->>>*/
        var sendForTesting = {
            eListener: FEATURE_TESTS.addEventListener,
            aElementsCapture: VARIABLE_CONTAINER.getAElements,
            inputFieldCapture: VARIABLE_CONTAINER.getTheInputField
        };
        return sendForTesting;
        /*<<<---------Sending_Data_for_Tests***********/
    })(jQuery);