/**
 * Created by stanimirov on 2/13/2015.
 */
var ADD_INPUT_VALUE_TO_RESULT_FIELD_ON_SUBMIT = (function($){
    'use strict';
    var FEATURE_TESTS = {
            addEventListener: !!window.addEventListener
        },
        VARIABLE_CONTAINER = {
            getTheForm: $('#months-form'),
            getTheInputField: $('#month-input-field'),
            getTheResultsField: $('#results-field'),
            getTheSuggestionWindow: $('#suggestionWindow')
        };

    if(FEATURE_TESTS.addEventListener){
        $(VARIABLE_CONTAINER.getTheForm).submit(function( event ){
            event = event || window.event;
            if ($(VARIABLE_CONTAINER.getTheInputField).val()) {
                event.preventDefault();
                VARIABLE_CONTAINER.getTheResultsField.html($(VARIABLE_CONTAINER. getTheInputField).val());
                $(VARIABLE_CONTAINER. getTheInputField).val('').focusout();
                $(VARIABLE_CONTAINER.getTheSuggestionWindow).addClass('hideWindow');
            }else{
                event.preventDefault();
                console.log('no data to store');
            }
        });
    }
    /*********Sending_Data_for_Tests----------->>>*/
    var sendDataForTests = {
        eListener: FEATURE_TESTS.addEventListener,
        formCapture: VARIABLE_CONTAINER.getTheForm,
        inputFieldCapture: VARIABLE_CONTAINER.getTheInputField,
        resultFieldCapture: VARIABLE_CONTAINER.getTheResultsField
    };
    return sendDataForTests;
    /*<<<---------Sending_Data_for_Tests***********/
})(jQuery);