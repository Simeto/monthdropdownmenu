/**
 * Created by stanimirov on 1/20/2015.
 */

var ADD_FIRST_MONTH_FROM_THE_SUGGESTION_LIST = (function($){
    'use strict';
    var VAR_CONTAINER = {
        getTheSuggestionWindow: $('#suggestionWindow'),
        getTheInputField: $('#month-input-field'),
        addFirstMonthDelayTime: 350,
        getTheForm: $('#months-form')
    }, FUNC_CONTAINER = {
        addCursorInPosition: function(){
            $(VAR_CONTAINER.getTheInputField).keyup(function( event ){
                event = event || window.event;
                /***Down Keyboard Arrow***/
                if(event.keyCode != 40) {
                    if (!$(VAR_CONTAINER.getTheSuggestionWindow).hasClass('hideWindow')) {
                        if ($(VAR_CONTAINER.getTheInputField).val()) {
                            setTimeout(function () {
                                $(VAR_CONTAINER.getTheInputField).val(($('.suggestion_window_a:first').text())).focus();
                            }, VAR_CONTAINER.addFirstMonthDelayTime);
                        } else if (!$(VAR_CONTAINER.getTheInputField).val()) {
                            console.log('The suggestion list is empty');
                        }
                    }
                }
            });

        }
    };
    FUNC_CONTAINER.addCursorInPosition();
    /*********Sending_Data_for_Tests----------->>>*/
    var sendForTesting = {
        suggestionListCapture: VAR_CONTAINER.getTheSuggestionWindow,
        inputFieldCapture: VAR_CONTAINER.getTheInputField,
        addFirstMonthDelayTimeCapture: VAR_CONTAINER.addFirstMonthDelayTime,
        getTheFormCapture: VAR_CONTAINER.getTheForm
    };
    return sendForTesting;
})(jQuery);