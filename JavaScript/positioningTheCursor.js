/**
 * Created by stanimirov on 2/13/2015.
 */
var POSITIONING_THE_CURSOR = (function($){
    'use strict';
    var FEATURE_TESTS = {
            addEventListener: !!window.addEventListener
        },
         VAR_HOLDER = {
             getTheInputField: $('#month-input-field'),
             getTheSuggestionWindow:$('#suggestionWindow'),
             getSuggestionAElements: $('.suggestion_window_a'),
             setCursorPositionDelayTime: 300
        },
         FUNC_HOLDER = {
            getCursorPosition: function(elem) {
                var input = elem.get(0);
                if (!input){ return; } // No (input) element found
                if ('selectionStart' in input) {
                    // Standard-compliant browsers
                    return input.selectionStart;
                } else if (document.selection) {
                    // IE
                    input.focus();
                    var sel = document.selection.createRange();
                    var selLen = document.selection.createRange().text.length;
                    sel.moveStart('character', -input.value.length);
                    return sel.text.length - selLen;
                }
            },
            setCursor: function (el,st,end) {
                if(el.setSelectionRange) {
                    el.focus();
                    el.setSelectionRange(st,end);
                }
                else {
                    // IE
                    if(el.createTextRange) {
                        var range = el.createTextRange();
                        range.collapse(true);
                        range.moveEnd(end);
                        range.moveStart(st);
                        range.select();
                    }
                }
            },
            addCursorInPosition: function() {
                if(FEATURE_TESTS.addEventListener){
                    $(VAR_HOLDER.getTheInputField).keyup(function ( event ) {
                        event = event || window.event;
                        /***Down Keyboard Arrow***/
                        if(event.keyCode != 40) {
                            if (!$(VAR_HOLDER.getTheSuggestionWindow).hasClass('hideWindow')) {
                                if ($(VAR_HOLDER.getTheInputField).val()) {
                                    setTimeout(function () {
                                        var positionReturn = FUNC_HOLDER.getCursorPosition($(VAR_HOLDER.getTheInputField));
                                        $(VAR_HOLDER.getTheInputField).focus(function () {
                                            FUNC_HOLDER.setCursor(document.getElementById('month-input-field'), positionReturn, 9);
                                        });
                                    }, VAR_HOLDER.setCursorPositionDelayTime);
                                } else if (!$(VAR_HOLDER.getTheInputField).val()) {
                                    console.log('The suggestion list is empty');
                                }
                            }
                        }
                    });
                }
            }
        };
    FUNC_HOLDER.addCursorInPosition();

    /*********Sending_Data_for_Tests----------->>>*/
    var sendForTesting = {
        inputFieldCapture: VAR_HOLDER.getTheInputField,
        setCursorDelayTimeCapture: VAR_HOLDER.setCursorPositionDelayTime
    };
    return sendForTesting;
    /*<<<---------Sending_Data_for_Tests***********/
})(jQuery);