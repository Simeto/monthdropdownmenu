/**
 * Created by stanimirov on 2/24/2015.
 */

var ADD_ESCAPE_KEY_BEHAVIOUR = (function($){
    'use strict';

     function onKeyClick (){
         var getTheInputField = $('#month-input-field'),
             getTheSuggestionWindow = $('#suggestionWindow'),
             addEventListener = !!window.addEventListener,
             getSuggestionAElements = $('.suggestion_window_a');

        if(addEventListener){
            $( getTheInputField  ).keyup(function( event ){
                event = event || window.event;
                /***Keyboard Escape Key***/
                if(event.keyCode == 27){
                    $( getTheInputField ).val('').focus();
                    $( getTheSuggestionWindow ).addClass('hideWindow');
                }
                /***Keyboard Backspace Key***/
                if(event.keyCode == 8){
                    if(!getTheInputField.val()){
                        $( getTheSuggestionWindow ).addClass('hideWindow');
                    }
                }
                setTimeout(function(){
                    if(!$(getTheInputField).val()){
                        $( getTheSuggestionWindow ).addClass('hideWindow');
                    }
                },375);
            });


        }


     }
onKeyClick();
})(jQuery);

