/**
 * Created by stanimirov on 3/30/2015.
 */

var DISPLAY_NOTIFICATION = (function ($) {
    'use strict';

    var theBody = document.body;
    theBody.addEventListener('notify', notify, false);

    function notify(e) {
        e = e || window.event;
        /* sets default values to target, message, options and type
         (so that the notification may not explode in your face) */
        var target            = e.detail.element || $,
            message           = e.detail.message || '',
            options           = e.detail.options || {};

        options.className = e.detail.type || e.detail.options.className || 'success';

        target.notify(message, options);
    }
})(jQuery);
