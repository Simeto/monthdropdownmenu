/**
 * Created by stanimirov on 2/13/2015.
 */

describe("POSITIONING THE CURSOR", function(){
        var $passFixture, $inputField, $delayTime;
        beforeEach(function(){
            jasmine.getFixtures.fixturesPath = "../MonthDropDownMenu";
            $passFixture = loadFixtures("index.html");
            /*Received Data from JS module files*/
            $inputField = $(POSITIONING_THE_CURSOR.inputFieldCapture);
            $delayTime = 300;
        });

       /*Input Field*/
        describe("Input Field", function(){
            it("Should Be Defined", function(){
                var specHolder = $inputField;
                expect(specHolder).toBeDefined()
            });
        });

        /*Delay Time*/
        describe("Cursor Positioning Delay", function(){
            it("Should Be Defined", function(){
                var specHolder = $inputField;
                expect(specHolder).toBeDefined();
            });

            it("Should Be Equal To 200ms", function(){
                var specHolder = $delayTime;
                expect(specHolder).toEqual(300);
            });
        });
});