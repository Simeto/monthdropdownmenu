describe("ADD MONTH FROM THE DROP DOWN LIST", function(){
    var $passFixture, $eListener, $inputField, $aElement ;

    beforeEach(function(){
        jasmine.getFixtures().fixturesPath = "../MonthDropDownMenu";
        $passFixture = loadFixtures("index.html");
        /*Received Data from JS module files*/
        $eListener = ADD_MONTH_FROM_DROP_DOWN_LIST.eListener;
        $inputField = $(ADD_MONTH_FROM_DROP_DOWN_LIST.inputFieldCapture);
        $aElement = $(ADD_MONTH_FROM_DROP_DOWN_LIST.aElementsCapture);
    });
    /*Event Listener*/
    describe('Event Listener', function(){
        it('Listener Should Be Defined', function(){
            var specHolder = $eListener;
            expect(specHolder).toBeDefined();
        });

        it('Listener Should Be Equal To Listener', function(){
            var specHolder = $eListener;
            expect(specHolder).toEqual(!!window.addEventListener);
        });
    });
    /*Month Input Field*/
    describe('Input Field', function(){
        it('Input Field Should Be Defined', function(){
            var specHolder = $inputField;
            expect(specHolder).toBeDefined()
        });

        it("Input Field Should Be Text Input", function(){
            var specHolder = $inputField;
            expect(specHolder).toHaveAttr("type", "text");
        });

        it("Input field should be in the DOM", function() {
            var specHolder = $inputField;
            expect(specHolder).toBeInDOM();
        });

        it("Input Field Should has an ID equal to 'month-input-field'", function() {
            var specHolder = $inputField;
            expect(specHolder).toHaveId("month-input-field")
        });

       /* it("Input Field Should be focused", function() {
            expect($inputField).toBeFocused()
        });*/
    });
    /*Drop Down Menu LI Elements*/
    describe('A Elements', function(){
        it('Li Capture Should Be Defined', function(){
            var specHolder = $aElement;
            expect(specHolder).toBeDefined()
        });

        it('A elements should be in the DOM', function(){
            var specHolder = $aElement;
            expect(specHolder).toBeInDOM();
        });

        it("Should contain 'A' elements", function(){
            var specHolder = $aElement;
            expect(specHolder).toContainHtml('</li>')
        });

        it("Should contain 'A' elements", function(){
            var specHolder = $aElement;
            expect(specHolder).toContainHtml('</a>')
        });
    });
});
