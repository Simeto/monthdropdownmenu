

describe("ADD FIRST MONTH FROM THE SUGGESTION WINDOW", function(){
    var $passFixture, $suggestionList, $inputField, $delayTime, $formCapture;

    beforeEach(function(){
        jasmine.getFixtures().fixturesPath = "../MonthDropDownMenu";
        $passFixture = loadFixtures("index.html");
        /*Received Data from JS module files*/
        $suggestionList = $(ADD_FIRST_MONTH_FROM_THE_SUGGESTION_LIST.suggestionListCapture);
        $inputField = $(ADD_FIRST_MONTH_FROM_THE_SUGGESTION_LIST.inputFieldCapture);
        $delayTime = 350;
        $formCapture = $(ADD_FIRST_MONTH_FROM_THE_SUGGESTION_LIST.getTheFormCapture);
    });
    /******Suggestion List******/
    describe("Suggestion List", function(){
        it("Suggestion list Should Be Defined", function(){
            var specHolder = $suggestionList;
            expect(specHolder).toBeDefined();
        });

        it("Suggestion List Should Be In The DOM", function(){
            var specHolder = $suggestionList;
            expect(specHolder).toBeInDOM();
        });

        it("Suggestion List Should Be Hidden", function(){
            var specHolder = $suggestionList;
            expect(specHolder).toHaveClass('hideWindow');
        });

        it("Suggestion List Should Contain UL With Class panel-dody", function(){
            var specHolder = $suggestionList;
            expect(specHolder).toContainElement('ul.panel-body')
        });
    });

    /******Input Field******/
    describe("Input Field", function(){
        it("Input Field Should Be Defined", function(){
            var specHolder = $inputField;
            expect(specHolder).toBeDefined();
        });

        it("Input Field Should Be In The DOM", function(){
            var specHolder = $inputField;
            expect(specHolder).toBeInDOM();
        });

        it("Input Field Should Be Input", function(){
            var specHolder = $inputField;
            expect(specHolder).toHaveAttr("type", "text");
        });

        it("Input Key Press", function(){
            var specHolder = $inputField;
            var spyEvent = spyOnEvent(specHolder , 'keypress' );
            specHolder.trigger( 'keypress' );
            expect( 'keypress' ).toHaveBeenTriggeredOn( specHolder );
        });
    });
    /******Delay Time******/
    describe("First Month Delay Time", function(){
        it("Delay Time Should Be Defined", function(){
            var specHolder = $delayTime;
            expect(specHolder).toBeDefined();
        });

        it("Delay Time Should Be Equal To 250", function(){
            var specHolder = $delayTime;
            expect(specHolder).toEqual(350);
        });
    });
    /******Form******/
    describe("long asynchronous specs", function() {
        it("Form Submit", function(){

            var specHolder = $formCapture;
            var spyEvent = spyOnEvent(specHolder , 'submit' );
            specHolder.trigger( 'submit' );
            expect( 'submit' ).toHaveBeenTriggeredOn( specHolder );

        });
    });


});