/**
 * Created by stanimirov on 2/13/2015.
 */
describe("ADD INPUT VALUE TO RESULT FIELD ON SUBMIT", function(){
    var $passFixture, $eListener, $formCapture, $inputField, $resultField;

    beforeEach(function(){
        jasmine.getFixtures().fixturesPath = "../MonthDropDownMenu";
        $passFixture = loadFixtures("index.html");
        /*Received Data from JS module files*/
        $eListener = ADD_INPUT_VALUE_TO_RESULT_FIELD_ON_SUBMIT.eListener;
        $formCapture = $(ADD_INPUT_VALUE_TO_RESULT_FIELD_ON_SUBMIT.formCapture);
        $inputField  = $(ADD_INPUT_VALUE_TO_RESULT_FIELD_ON_SUBMIT.inputFieldCapture);
        $resultField = $(ADD_INPUT_VALUE_TO_RESULT_FIELD_ON_SUBMIT.resultFieldCapture);
    });
    /*Event Listener*/
    describe("Event Listener", function(){
        it("Listener Should Be Defined", function(){
            var specHolder = $eListener;
            expect(specHolder).toBeDefined();
        });

        it("Listener Should Be Equal To Listener", function(){
            var specHolder = $eListener;
            expect(specHolder).toEqual(!!window.addEventListener);
        })
    });
    /*Form*/
    describe("Months Form", function(){
        it("Form Should Be Defined", function(){
            var specHolder = $formCapture;
            expect(specHolder ).toBeDefined();
        });

        it("Form Should Be In The DOM", function(){
            var specHolder = $formCapture;
            expect(specHolder ).toBeInDOM();
        });

        it("Form Submit", function(){
            var specHolder = $formCapture;
            var spyEvent = spyOnEvent(specHolder , 'submit' );
            specHolder.trigger( 'submit' );
            expect( 'submit' ).toHaveBeenTriggeredOn( specHolder );
        });
    });

    /*Input Field*/
    describe("Input Field", function(){
        it("Input Should Be Defined", function(){
            var specHolder = $inputField;
            expect(specHolder).toBeDefined();
        });

        it("Input Should Be In The DOM", function(){
            var specHolder = $inputField;
            expect(specHolder).toBeInDOM();
        });

        it("Should Be Text Input", function(){
            var specHolder = $inputField;
            expect(specHolder).toHaveAttr("type", "text")
        });

    });

    /*Results Field*/
    describe("Result Field", function(){
        it("Result Field Should Be Defined", function(){
            var specHolder = $resultField;
            expect(specHolder).toBeDefined();
        });

        it("Result Field Should Be In The DOM", function(){
            var specHolder = $resultField;
            expect(specHolder).toBeInDOM();
        })
    });

});