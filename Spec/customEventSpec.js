/**
 * Created by stanimirov on 2/18/2015.
 */

describe("CUSTOM EVENT",function(){
    var $passFixture, $theBody,$theSubmitButton;

    beforeEach(function(){
        jasmine.getFixtures().fixturesPath = "../MonthDropDownMenu";
        $passFixture = loadFixtures("index.html");
        /*Received Data from JS module files*/
        $theBody = document.body;
        $theSubmitButton = $('#the-submit-button');
    });

    /*Custom Event*/
    describe("Custom Event Test", function() {
        it("Event Should Be Triggered", function(){
            var specHolder = $theBody;
            var submitButton = $theSubmitButton;
            var spyEvent = spyOnEvent(specHolder , 'newMessage' );
            submitButton.trigger( 'click' );
            expect( 'newMessage' ).toHaveBeenTriggeredOn(specHolder);
        });
    });

});