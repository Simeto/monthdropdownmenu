/**
 * Created by stanimirov on 1/20/2015.
 */

var ADD_FIRST_MONTH_FROM_THE_SUGGESTION_LIST = (function($){
    'use strict';
    var VAR_CONTAINER = {
        getTheSuggestionWindow: $('#suggestionWindow'),
        getTheInputField: $('#month-input-field'),
        addFirstMonthDelayTime: 350,
        getTheForm: $('#months-form')
    }, FUNC_CONTAINER = {
        addCursorInPosition: function(){
            $(VAR_CONTAINER.getTheInputField).keyup(function( event ){
                event = event || window.event;
                /***Down Keyboard Arrow***/
                if(event.keyCode != 40) {
                    if (!$(VAR_CONTAINER.getTheSuggestionWindow).hasClass('hideWindow')) {
                        if ($(VAR_CONTAINER.getTheInputField).val()) {
                            setTimeout(function () {
                                $(VAR_CONTAINER.getTheInputField).val(($('.suggestion_window_a:first').text())).focus();
                            }, VAR_CONTAINER.addFirstMonthDelayTime);
                        } else if (!$(VAR_CONTAINER.getTheInputField).val()) {
                            console.log('The suggestion list is empty');
                        }
                    }
                }
            });

        }
    };
    FUNC_CONTAINER.addCursorInPosition();
    /*********Sending_Data_for_Tests----------->>>*/
    var sendForTesting = {
        suggestionListCapture: VAR_CONTAINER.getTheSuggestionWindow,
        inputFieldCapture: VAR_CONTAINER.getTheInputField,
        addFirstMonthDelayTimeCapture: VAR_CONTAINER.addFirstMonthDelayTime,
        getTheFormCapture: VAR_CONTAINER.getTheForm
    };
    return sendForTesting;
})(jQuery);;/**
 * Created by stanimirov on 2/13/2015.
 */
var ADD_INPUT_VALUE_TO_RESULT_FIELD_ON_SUBMIT = (function($){
    'use strict';
    var FEATURE_TESTS = {
            addEventListener: !!window.addEventListener
        },
        VARIABLE_CONTAINER = {
            getTheForm: $('#months-form'),
            getTheInputField: $('#month-input-field'),
            getTheResultsField: $('#results-field'),
            getTheSuggestionWindow: $('#suggestionWindow')
        };

    if(FEATURE_TESTS.addEventListener){
        $(VARIABLE_CONTAINER.getTheForm).submit(function( event ){
            event = event || window.event;
            if ($(VARIABLE_CONTAINER.getTheInputField).val()) {
                event.preventDefault();
                VARIABLE_CONTAINER.getTheResultsField.html($(VARIABLE_CONTAINER. getTheInputField).val());
                $(VARIABLE_CONTAINER. getTheInputField).val('').focusout();
                $(VARIABLE_CONTAINER.getTheSuggestionWindow).addClass('hideWindow');
            }else{
                event.preventDefault();
              /*  $('#month-input-field').notify(
                    'You can not put negative under the radical sign!!!',
                    { position: 'right top',
                        className: 'info' }
                );*/
                console.log('no data to store');
            }
        });
    }
    /*********Sending_Data_for_Tests----------->>>*/
    var sendDataForTests = {
        eListener: FEATURE_TESTS.addEventListener,
        formCapture: VARIABLE_CONTAINER.getTheForm,
        inputFieldCapture: VARIABLE_CONTAINER.getTheInputField,
        resultFieldCapture: VARIABLE_CONTAINER.getTheResultsField
    };
    return sendDataForTests;
    /*<<<---------Sending_Data_for_Tests***********/
})(jQuery);;/**
 * Created by stanimirov on 2/24/2015.
 */

var ADD_ESCAPE_KEY_BEHAVIOUR = (function($){
    'use strict';

     function onKeyClick (){
         var getTheInputField = $('#month-input-field'),
             getTheSuggestionWindow = $('#suggestionWindow'),
             addEventListener = !!window.addEventListener,
             getSuggestionAElements = $('.suggestion_window_a');

        if(addEventListener){
            $( getTheInputField  ).keyup(function( event ){
                event = event || window.event;
                /***Keyboard Escape Key***/
                if(event.keyCode == 27){
                    $( getTheInputField ).val('').focus();
                    $( getTheSuggestionWindow ).addClass('hideWindow');
                }
                /***Keyboard Backspace Key***/
                if(event.keyCode == 8){
                    if(!getTheInputField.val()){
                        $( getTheSuggestionWindow ).addClass('hideWindow');
                    }
                }
                setTimeout(function(){
                    if(!$(getTheInputField).val()){
                        $( getTheSuggestionWindow ).addClass('hideWindow');
                    }
                },375);
            });


        }


     }
onKeyClick();
})(jQuery);

;/**
 * Created by stanimirov on 2/13/2015.
 */
var ADD_MONTH_FROM_DROP_DOWN_LIST = (function($){
        'use strict';

        var FEATURE_TESTS = {
                addEventListener: !!window.addEventListener

            },
            VARIABLE_CONTAINER = {
                getAElements: $('a'),
                getTheInputField: $('#month-input-field')
            };
        if(FEATURE_TESTS.addEventListener){
            $(VARIABLE_CONTAINER.getAElements).click(function(){
                VARIABLE_CONTAINER.getTheInputField.val($(this).text());
                VARIABLE_CONTAINER.getTheInputField.focus();
            });
        }
        /*********Sending_Data_for_Tests----------->>>*/
        var sendForTesting = {
            eListener: FEATURE_TESTS.addEventListener,
            aElementsCapture: VARIABLE_CONTAINER.getAElements,
            inputFieldCapture: VARIABLE_CONTAINER.getTheInputField
        };
        return sendForTesting;
        /*<<<---------Sending_Data_for_Tests***********/
    })(jQuery);;/**
 * Created by stanimirov on 2/13/2015.
 */
var AUTOCOMPLETE_FOR_MONTHS = (function($){
    'use strict';
    var FEATURE_TESTS = {
            addEventListener: !!window.addEventListener
        },
        VARIABLE_HOLDER = {
            getTheInputField: $('#month-input-field'),
            getTheSuggestionWindow: $('#suggestionWindow'),   /*Used to send element for tests*/
            getTheAutocompletePanel: $('#autocomplete-panel'),
            suggestionDelayTime: 400
        },
        ARRAY_HOLDER = {
            monthsArray: ['January','February','March','April','May','June','July','August','September','October','November','December'
            ]
        },
        FUNCTION_HOLDER = {
            arrayToLowerCase: $.map(ARRAY_HOLDER.monthsArray, function (item, index) {
                return  item.toLowerCase().split();
            }),
            monthsAutocompleteIteration:  function (){
                if(FEATURE_TESTS.addEventListener){
                    if($(VARIABLE_HOLDER.getTheInputField).keyup(function( event ){
                            event = event || window.event;
                            /***Down Keyboard Arrow***/
                            if(event.keyCode != 40) {
                                VARIABLE_HOLDER.getTheAutocompletePanel.html('');
                                if ($(VARIABLE_HOLDER.getTheInputField).val()) {
                                    var inputValToLowerCase = $(VARIABLE_HOLDER.getTheInputField).val().toLowerCase();
                                    var i;
                                    var len;
                                    var pattern = new RegExp("\\b(" + inputValToLowerCase + ")");
                                    for (i = 0, len = FUNCTION_HOLDER.arrayToLowerCase.length; i < len; i += 1) {
                                        if (pattern.exec(FUNCTION_HOLDER.arrayToLowerCase[i])) {
                                            VARIABLE_HOLDER.getTheAutocompletePanel.append('<li class="suggestion_window_li" >' + '<a class="suggestion_window_a" href="#">' + FUNCTION_HOLDER.arrayToLowerCase[i] + '</a>' + '</li>');
                                        }
                                    }
                                }
                            }
                        })){
                    }
                }
            },
            suggestionsTimeout: function(){
                setTimeout(function(){
                    FUNCTION_HOLDER.monthsAutocompleteIteration();
                },VARIABLE_HOLDER.suggestionDelayTime);
            }
        };
    FUNCTION_HOLDER.suggestionsTimeout();

    /*********Sending_Data_for_Tests----------->>>*/
    var sendForTesting = {
        eListener: FEATURE_TESTS.addEventListener,
        inputFieldCapture: VARIABLE_HOLDER.getTheInputField,
        suggestionWindowCapture:  VARIABLE_HOLDER.getTheSuggestionWindow,
        autocompletePannelCapture: VARIABLE_HOLDER.getTheAutocompletePanel,
        suggestionDelayTimeCapture: VARIABLE_HOLDER.suggestionDelayTime,
        monthsArrayCapture: ARRAY_HOLDER.monthsArray,
        arrayToLowercaseCapture: FUNCTION_HOLDER.arrayToLowerCase,
        monthsAutocompleteIterationCapture: FUNCTION_HOLDER.monthsAutocompleteIteration,
        suggestionsTimeoutCapture: FUNCTION_HOLDER.suggestionsTimeout
    };
    return sendForTesting;
    /*<<<---------Sending_Data_for_Tests***********/
})(jQuery);;/**
 * Created by stanimirov on 3/16/2015.
 */
(function($){
    $.fn.focusTextToEnd = function(){
        this.focus();
        var $thisVal = this.val();
        this.val('').val($thisVal);
        return this;
    };
}(jQuery));;/**
 * Created by stanimirov on 3/30/2015.
 */

(function($){
    document.body.addEventListener('notify', notify, false);

    function notify(e) {
            /* e.element must be with a jQuery selector!!! */
            var target = e.detail.element || $(document.body),
                message = e.detail.message || 'foo',
                options = e.detail.options || {},
                type = e.detail.type || 'success';
            target.notify(message, type, options);
    }
})(jQuery);
;/** Notify.js - v0.3.1 - 2014/06/29
 * http://notifyjs.com/
 * Copyright (c) 2014 Jaime Pillora - MIT
 */
(function(window,document,$,undefined) {
    'use strict';

    var Notification, addStyle, blankFieldName, coreStyle, createElem, defaults, encode, find, findFields, getAnchorElement, getStyle, globalAnchors, hAligns, incr, inherit, insertCSS, mainPositions, opposites, parsePosition, pluginClassName, pluginName, pluginOptions, positions, realign, stylePrefixes, styles, vAligns,
        __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

    pluginName = 'notify';

    pluginClassName = pluginName + 'js';

    blankFieldName = pluginName + "!blank";

    positions = {
        t: 'top',
        m: 'middle',
        b: 'bottom',
        l: 'left',
        c: 'center',
        r: 'right'
    };

    hAligns = ['l', 'c', 'r'];

    vAligns = ['t', 'm', 'b'];

    mainPositions = ['t', 'b', 'l', 'r'];

    opposites = {
        t: 'b',
        m: null,
        b: 't',
        l: 'r',
        c: null,
        r: 'l'
    };

    parsePosition = function(str) {
        var pos;
        pos = [];
        $.each(str.split(/\W+/), function(i, word) {
            var w;
            w = word.toLowerCase().charAt(0);
            if (positions[w]) {
                return pos.push(w);
            }
        });
        return pos;
    };

    styles = {};

    coreStyle = {
        name: 'core',
        html: "<div class=\"" + pluginClassName + "-wrapper\">\n  <div class=\"" + pluginClassName + "-arrow\"></div>\n  <div class=\"" + pluginClassName + "-container\"></div>\n</div>",
        css: "." + pluginClassName + "-corner {\n  position: fixed;\n  margin: 5px;\n  z-index: 1050;\n}\n\n." + pluginClassName + "-corner ." + pluginClassName + "-wrapper,\n." + pluginClassName + "-corner ." + pluginClassName + "-container {\n  position: relative;\n  display: block;\n  height: inherit;\n  width: inherit;\n  margin: 3px;\n}\n\n." + pluginClassName + "-wrapper {\n  z-index: 1;\n  position: absolute;\n  display: inline-block;\n  height: 0;\n  width: 0;\n}\n\n." + pluginClassName + "-container {\n  display: none;\n  z-index: 1;\n  position: absolute;\n}\n\n." + pluginClassName + "-hidable {\n  cursor: pointer;\n}\n\n[data-notify-text],[data-notify-html] {\n  position: relative;\n}\n\n." + pluginClassName + "-arrow {\n  position: absolute;\n  z-index: 2;\n  width: 0;\n  height: 0;\n}"
    };

    stylePrefixes = {
        "border-radius": ["-webkit-", "-moz-"]
    };

    getStyle = function(name) {
        return styles[name];
    };

    addStyle = function(name, def) {
        var cssText, elem, fields, _ref;
        if (!name) {
            throw "Missing Style name";
        }
        if (!def) {
            throw "Missing Style definition";
        }
        if (!def.html) {
            throw "Missing Style HTML";
        }
        if ((_ref = styles[name]) != null ? _ref.cssElem : void 0) {
            if (window.console) {
                console.warn("" + pluginName + ": overwriting style '" + name + "'");
            }
            styles[name].cssElem.remove();
        }
        def.name = name;
        styles[name] = def;
        cssText = "";
        if (def.classes) {
            $.each(def.classes, function(className, props) {
                cssText += "." + pluginClassName + "-" + def.name + "-" + className + " {\n";
                $.each(props, function(name, val) {
                    if (stylePrefixes[name]) {
                        $.each(stylePrefixes[name], function(i, prefix) {
                            return cssText += "  " + prefix + name + ": " + val + ";\n";
                        });
                    }
                    return cssText += "  " + name + ": " + val + ";\n";
                });
                return cssText += "}\n";
            });
        }
        if (def.css) {
            cssText += "/* styles for " + def.name + " */\n" + def.css;
        }
        if (cssText) {
            def.cssElem = insertCSS(cssText);
            def.cssElem.attr('id', "notify-" + def.name);
        }
        fields = {};
        elem = $(def.html);
        findFields('html', elem, fields);
        findFields('text', elem, fields);
        return def.fields = fields;
    };

    insertCSS = function(cssText) {
        var elem;
        elem = createElem("style");
        elem.attr('type', 'text/css');
        $("head").append(elem);
        try {
            elem.html(cssText);
        } catch (e) {
            elem[0].styleSheet.cssText = cssText;
        }
        return elem;
    };

    findFields = function(type, elem, fields) {
        var attr;
        if (type !== 'html') {
            type = 'text';
        }
        attr = "data-notify-" + type;
        return find(elem, "[" + attr + "]").each(function() {
            var name;
            name = $(this).attr(attr);
            if (!name) {
                name = blankFieldName;
            }
            return fields[name] = type;
        });
    };

    find = function(elem, selector) {
        if (elem.is(selector)) {
            return elem;
        } else {
            return elem.find(selector);
        }
    };

    pluginOptions = {
        clickToHide: true,
        autoHide: true,
        autoHideDelay: 5000,
        arrowShow: true,
        arrowSize: 5,
        breakNewLines: true,
        elementPosition: 'bottom',
        globalPosition: 'top right',
        style: 'bootstrap',
        className: 'error',
        showAnimation: 'slideDown',
        showDuration: 400,
        hideAnimation: 'slideUp',
        hideDuration: 200,
        gap: 5
    };

    inherit = function(a, b) {
        var F;
        F = function() {};
        F.prototype = a;
        return $.extend(true, new F(), b);
    };

    defaults = function(opts) {
        return $.extend(pluginOptions, opts);
    };

    createElem = function(tag) {
        return $("<" + tag + "></" + tag + ">");
    };

    globalAnchors = {};

    getAnchorElement = function(element) {
        var radios;
        if (element.is('[type=radio]')) {
            radios = element.parents('form:first').find('[type=radio]').filter(function(i, e) {
                return $(e).attr('name') === element.attr('name');
            });
            element = radios.first();
        }
        return element;
    };

    incr = function(obj, pos, val) {
        var opp, temp;
        if (typeof val === 'string') {
            val = parseInt(val, 10);
        } else if (typeof val !== 'number') {
            return;
        }
        if (isNaN(val)) {
            return;
        }
        opp = positions[opposites[pos.charAt(0)]];
        temp = pos;
        if (obj[opp] !== undefined) {
            pos = positions[opp.charAt(0)];
            val = -val;
        }
        if (obj[pos] === undefined) {
            obj[pos] = val;
        } else {
            obj[pos] += val;
        }
        return null;
    };

    realign = function(alignment, inner, outer) {
        if (alignment === 'l' || alignment === 't') {
            return 0;
        } else if (alignment === 'c' || alignment === 'm') {
            return outer / 2 - inner / 2;
        } else if (alignment === 'r' || alignment === 'b') {
            return outer - inner;
        }
        throw "Invalid alignment";
    };

    encode = function(text) {
        encode.e = encode.e || createElem("div");
        return encode.e.text(text).html();
    };

    Notification = (function() {

        function Notification(elem, data, options) {
            if (typeof options === 'string') {
                options = {
                    className: options
                };
            }
            this.options = inherit(pluginOptions, $.isPlainObject(options) ? options : {});
            this.loadHTML();
            this.wrapper = $(coreStyle.html);
            if (this.options.clickToHide) {
                this.wrapper.addClass("" + pluginClassName + "-hidable");
            }
            this.wrapper.data(pluginClassName, this);
            this.arrow = this.wrapper.find("." + pluginClassName + "-arrow");
            this.container = this.wrapper.find("." + pluginClassName + "-container");
            this.container.append(this.userContainer);
            if (elem && elem.length) {
                this.elementType = elem.attr('type');
                this.originalElement = elem;
                this.elem = getAnchorElement(elem);
                this.elem.data(pluginClassName, this);
                this.elem.before(this.wrapper);
            }
            this.container.hide();
            this.run(data);
        }

        Notification.prototype.loadHTML = function() {
            var style;
            style = this.getStyle();
            this.userContainer = $(style.html);
            return this.userFields = style.fields;
        };

        Notification.prototype.show = function(show, userCallback) {
            var args, callback, elems, fn, hidden,
                _this = this;
            callback = function() {
                if (!show && !_this.elem) {
                    _this.destroy();
                }
                if (userCallback) {
                    return userCallback();
                }
            };
            hidden = this.container.parent().parents(':hidden').length > 0;
            elems = this.container.add(this.arrow);
            args = [];
            if (hidden && show) {
                fn = 'show';
            } else if (hidden && !show) {
                fn = 'hide';
            } else if (!hidden && show) {
                fn = this.options.showAnimation;
                args.push(this.options.showDuration);
            } else if (!hidden && !show) {
                fn = this.options.hideAnimation;
                args.push(this.options.hideDuration);
            } else {
                return callback();
            }
            args.push(callback);
            return elems[fn].apply(elems, args);
        };

        Notification.prototype.setGlobalPosition = function() {
            var align, anchor, css, key, main, pAlign, pMain, _ref;
            _ref = this.getPosition(), pMain = _ref[0], pAlign = _ref[1];
            main = positions[pMain];
            align = positions[pAlign];
            key = pMain + "|" + pAlign;
            anchor = globalAnchors[key];
            if (!anchor) {
                anchor = globalAnchors[key] = createElem("div");
                css = {};
                css[main] = 0;
                if (align === 'middle') {
                    css.top = '45%';
                } else if (align === 'center') {
                    css.left = '45%';
                } else {
                    css[align] = 0;
                }
                anchor.css(css).addClass("" + pluginClassName + "-corner");
                $("body").append(anchor);
            }
            return anchor.prepend(this.wrapper);
        };

        Notification.prototype.setElementPosition = function() {
            var arrowColor, arrowCss, arrowSize, color, contH, contW, css, elemH, elemIH, elemIW, elemPos, elemW, gap, mainFull, margin, opp, oppFull, pAlign, pArrow, pMain, pos, posFull, position, wrapPos, _i, _j, _len, _len1, _ref;
            position = this.getPosition();
            pMain = position[0], pAlign = position[1], pArrow = position[2];
            elemPos = this.elem.position();
            elemH = this.elem.outerHeight();
            elemW = this.elem.outerWidth();
            elemIH = this.elem.innerHeight();
            elemIW = this.elem.innerWidth();
            wrapPos = this.wrapper.position();
            contH = this.container.height();
            contW = this.container.width();
            mainFull = positions[pMain];
            opp = opposites[pMain];
            oppFull = positions[opp];
            css = {};
            css[oppFull] = pMain === 'b' ? elemH : pMain === 'r' ? elemW : 0;
            incr(css, 'top', elemPos.top - wrapPos.top);
            incr(css, 'left', elemPos.left - wrapPos.left);
            _ref = ['top', 'left'];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                pos = _ref[_i];
                margin = parseInt(this.elem.css("margin-" + pos), 10);
                if (margin) {
                    incr(css, pos, margin);
                }
            }
            gap = Math.max(0, this.options.gap - (this.options.arrowShow ? arrowSize : 0));
            incr(css, oppFull, gap);
            if (!this.options.arrowShow) {
                this.arrow.hide();
            } else {
                arrowSize = this.options.arrowSize;
                arrowCss = $.extend({}, css);
                arrowColor = this.userContainer.css("border-color") || this.userContainer.css("background-color") || 'white';
                for (_j = 0, _len1 = mainPositions.length; _j < _len1; _j++) {
                    pos = mainPositions[_j];
                    posFull = positions[pos];
                    if (pos === opp) {
                        continue;
                    }
                    color = posFull === mainFull ? arrowColor : 'transparent';
                    arrowCss["border-" + posFull] = "" + arrowSize + "px solid " + color;
                }
                incr(css, positions[opp], arrowSize);
                if (__indexOf.call(mainPositions, pAlign) >= 0) {
                    incr(arrowCss, positions[pAlign], arrowSize * 2);
                }
            }
            if (__indexOf.call(vAligns, pMain) >= 0) {
                incr(css, 'left', realign(pAlign, contW, elemW));
                if (arrowCss) {
                    incr(arrowCss, 'left', realign(pAlign, arrowSize, elemIW));
                }
            } else if (__indexOf.call(hAligns, pMain) >= 0) {
                incr(css, 'top', realign(pAlign, contH, elemH));
                if (arrowCss) {
                    incr(arrowCss, 'top', realign(pAlign, arrowSize, elemIH));
                }
            }
            if (this.container.is(":visible")) {
                css.display = 'block';
            }
            this.container.removeAttr('style').css(css);
            if (arrowCss) {
                return this.arrow.removeAttr('style').css(arrowCss);
            }
        };

        Notification.prototype.getPosition = function() {
            var pos, text, _ref, _ref1, _ref2, _ref3, _ref4, _ref5;
            text = this.options.position || (this.elem ? this.options.elementPosition : this.options.globalPosition);
            pos = parsePosition(text);
            if (pos.length === 0) {
                pos[0] = 'b';
            }
            if (_ref = pos[0], __indexOf.call(mainPositions, _ref) < 0) {
                throw "Must be one of [" + mainPositions + "]";
            }
            if (pos.length === 1 || ((_ref1 = pos[0], __indexOf.call(vAligns, _ref1) >= 0) && (_ref2 = pos[1], __indexOf.call(hAligns, _ref2) < 0)) || ((_ref3 = pos[0], __indexOf.call(hAligns, _ref3) >= 0) && (_ref4 = pos[1], __indexOf.call(vAligns, _ref4) < 0))) {
                pos[1] = (_ref5 = pos[0], __indexOf.call(hAligns, _ref5) >= 0) ? 'm' : 'l';
            }
            if (pos.length === 2) {
                pos[2] = pos[1];
            }
            return pos;
        };

        Notification.prototype.getStyle = function(name) {
            var style;
            if (!name) {
                name = this.options.style;
            }
            if (!name) {
                name = 'default';
            }
            style = styles[name];
            if (!style) {
                throw "Missing style: " + name;
            }
            return style;
        };

        Notification.prototype.updateClasses = function() {
            var classes, style;
            classes = ['base'];
            if ($.isArray(this.options.className)) {
                classes = classes.concat(this.options.className);
            } else if (this.options.className) {
                classes.push(this.options.className);
            }
            style = this.getStyle();
            classes = $.map(classes, function(n) {
                return "" + pluginClassName + "-" + style.name + "-" + n;
            }).join(' ');
            return this.userContainer.attr('class', classes);
        };

        Notification.prototype.run = function(data, options) {
            var d, datas, name, type, value,
                _this = this;
            if ($.isPlainObject(options)) {
                $.extend(this.options, options);
            } else if ($.type(options) === 'string') {
                this.options.className = options;
            }
            if (this.container && !data) {
                this.show(false);
                return;
            } else if (!this.container && !data) {
                return;
            }
            datas = {};
            if ($.isPlainObject(data)) {
                datas = data;
            } else {
                datas[blankFieldName] = data;
            }
            for (name in datas) {
                d = datas[name];
                type = this.userFields[name];
                if (!type) {
                    continue;
                }
                if (type === 'text') {
                    d = encode(d);
                    if (this.options.breakNewLines) {
                        d = d.replace(/\n/g, '<br/>');
                    }
                }
                value = name === blankFieldName ? '' : '=' + name;
                find(this.userContainer, "[data-notify-" + type + value + "]").html(d);
            }
            this.updateClasses();
            if (this.elem) {
                this.setElementPosition();
            } else {
                this.setGlobalPosition();
            }
            this.show(true);
            if (this.options.autoHide) {
                clearTimeout(this.autohideTimer);
                return this.autohideTimer = setTimeout(function() {
                    return _this.show(false);
                }, this.options.autoHideDelay);
            }
        };

        Notification.prototype.destroy = function() {
            return this.wrapper.remove();
        };

        return Notification;

    })();

    $[pluginName] = function(elem, data, options) {
        if ((elem && elem.nodeName) || elem.jquery) {
            $(elem)[pluginName](data, options);
        } else {
            options = data;
            data = elem;
            new Notification(null, data, options);
        }
        return elem;
    };

    $.fn[pluginName] = function(data, options) {
        $(this).each(function() {
            var inst;
            inst = getAnchorElement($(this)).data(pluginClassName);
            if (inst) {
                return inst.run(data, options);
            } else {
                return new Notification($(this), data, options);
            }
        });
        return this;
    };

    $.extend($[pluginName], {
        defaults: defaults,
        addStyle: addStyle,
        pluginOptions: pluginOptions,
        getStyle: getStyle,
        insertCSS: insertCSS
    });

    $(function() {
        insertCSS(coreStyle.css).attr('id', 'core-notify');
        $(document).on('click', "." + pluginClassName + "-hidable", function(e) {
            return $(this).trigger('notify-hide');
        });
        return $(document).on('notify-hide', "." + pluginClassName + "-wrapper", function(e) {
            var _ref;
            return (_ref = $(this).data(pluginClassName)) != null ? _ref.show(false) : void 0;
        });
    });

}(window,document,jQuery));

$.notify.addStyle("bootstrap", {
    html: "<div>\n<span data-notify-text></span>\n</div>",
    classes: {
        base: {
            "font-weight": "bold",
            "padding": "8px 15px 8px 14px",
            "text-shadow": "0 1px 0 rgba(255, 255, 255, 0.5)",
            "background-color": "#fcf8e3",
            "border": "1px solid #fbeed5",
            "border-radius": "4px",
            "white-space": "nowrap",
            "padding-left": "25px",
            "background-repeat": "no-repeat",
            "background-position": "3px 7px"
        },
        error: {
            "color": "#B94A48",
            "background-color": "#F2DEDE",
            "border-color": "#EED3D7",
            "background-image": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAtRJREFUeNqkVc1u00AQHq+dOD+0poIQfkIjalW0SEGqRMuRnHos3DjwAH0ArlyQeANOOSMeAA5VjyBxKBQhgSpVUKKQNGloFdw4cWw2jtfMOna6JOUArDTazXi/b3dm55socPqQhFka++aHBsI8GsopRJERNFlY88FCEk9Yiwf8RhgRyaHFQpPHCDmZG5oX2ui2yilkcTT1AcDsbYC1NMAyOi7zTX2Agx7A9luAl88BauiiQ/cJaZQfIpAlngDcvZZMrl8vFPK5+XktrWlx3/ehZ5r9+t6e+WVnp1pxnNIjgBe4/6dAysQc8dsmHwPcW9C0h3fW1hans1ltwJhy0GxK7XZbUlMp5Ww2eyan6+ft/f2FAqXGK4CvQk5HueFz7D6GOZtIrK+srupdx1GRBBqNBtzc2AiMr7nPplRdKhb1q6q6zjFhrklEFOUutoQ50xcX86ZlqaZpQrfbBdu2R6/G19zX6XSgh6RX5ubyHCM8nqSID6ICrGiZjGYYxojEsiw4PDwMSL5VKsC8Yf4VRYFzMzMaxwjlJSlCyAQ9l0CW44PBADzXhe7xMdi9HtTrdYjFYkDQL0cn4Xdq2/EAE+InCnvADTf2eah4Sx9vExQjkqXT6aAERICMewd/UAp/IeYANM2joxt+q5VI+ieq2i0Wg3l6DNzHwTERPgo1ko7XBXj3vdlsT2F+UuhIhYkp7u7CarkcrFOCtR3H5JiwbAIeImjT/YQKKBtGjRFCU5IUgFRe7fF4cCNVIPMYo3VKqxwjyNAXNepuopyqnld602qVsfRpEkkz+GFL1wPj6ySXBpJtWVa5xlhpcyhBNwpZHmtX8AGgfIExo0ZpzkWVTBGiXCSEaHh62/PoR0p/vHaczxXGnj4bSo+G78lELU80h1uogBwWLf5YlsPmgDEd4M236xjm+8nm4IuE/9u+/PH2JXZfbwz4zw1WbO+SQPpXfwG/BBgAhCNZiSb/pOQAAAAASUVORK5CYII=)"
        },
        success: {
            "color": "#468847",
            "background-color": "#DFF0D8",
            "border-color": "#D6E9C6",
            "background-image": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAutJREFUeNq0lctPE0Ecx38zu/RFS1EryqtgJFA08YCiMZIAQQ4eRG8eDGdPJiYeTIwHTfwPiAcvXIwXLwoXPaDxkWgQ6islKlJLSQWLUraPLTv7Gme32zoF9KSTfLO7v53vZ3d/M7/fIth+IO6INt2jjoA7bjHCJoAlzCRw59YwHYjBnfMPqAKWQYKjGkfCJqAF0xwZjipQtA3MxeSG87VhOOYegVrUCy7UZM9S6TLIdAamySTclZdYhFhRHloGYg7mgZv1Zzztvgud7V1tbQ2twYA34LJmF4p5dXF1KTufnE+SxeJtuCZNsLDCQU0+RyKTF27Unw101l8e6hns3u0PBalORVVVkcaEKBJDgV3+cGM4tKKmI+ohlIGnygKX00rSBfszz/n2uXv81wd6+rt1orsZCHRdr1Imk2F2Kob3hutSxW8thsd8AXNaln9D7CTfA6O+0UgkMuwVvEFFUbbAcrkcTA8+AtOk8E6KiQiDmMFSDqZItAzEVQviRkdDdaFgPp8HSZKAEAL5Qh7Sq2lIJBJwv2scUqkUnKoZgNhcDKhKg5aH+1IkcouCAdFGAQsuWZYhOjwFHQ96oagWgRoUov1T9kRBEODAwxM2QtEUl+Wp+Ln9VRo6BcMw4ErHRYjH4/B26AlQoQQTRdHWwcd9AH57+UAXddvDD37DmrBBV34WfqiXPl61g+vr6xA9zsGeM9gOdsNXkgpEtTwVvwOklXLKm6+/p5ezwk4B+j6droBs2CsGa/gNs6RIxazl4Tc25mpTgw/apPR1LYlNRFAzgsOxkyXYLIM1V8NMwyAkJSctD1eGVKiq5wWjSPdjmeTkiKvVW4f2YPHWl3GAVq6ymcyCTgovM3FzyRiDe2TaKcEKsLpJvNHjZgPNqEtyi6mZIm4SRFyLMUsONSSdkPeFtY1n0mczoY3BHTLhwPRy9/lzcziCw9ACI+yql0VLzcGAZbYSM5CCSZg1/9oc/nn7+i8N9p/8An4JMADxhH+xHfuiKwAAAABJRU5ErkJggg==)"
        },
        info: {
            "color": "#3A87AD",
            "background-color": "#D9EDF7",
            "border-color": "#BCE8F1",
            "background-image": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QYFAhkSsdes/QAAA8dJREFUOMvVlGtMW2UYx//POaWHXg6lLaW0ypAtw1UCgbniNOLcVOLmAjHZolOYlxmTGXVZdAnRfXQm+7SoU4mXaOaiZsEpC9FkiQs6Z6bdCnNYruM6KNBw6YWewzl9z+sHImEWv+vz7XmT95f/+3/+7wP814v+efDOV3/SoX3lHAA+6ODeUFfMfjOWMADgdk+eEKz0pF7aQdMAcOKLLjrcVMVX3xdWN29/GhYP7SvnP0cWfS8caSkfHZsPE9Fgnt02JNutQ0QYHB2dDz9/pKX8QjjuO9xUxd/66HdxTeCHZ3rojQObGQBcuNjfplkD3b19Y/6MrimSaKgSMmpGU5WevmE/swa6Oy73tQHA0Rdr2Mmv/6A1n9w9suQ7097Z9lM4FlTgTDrzZTu4StXVfpiI48rVcUDM5cmEksrFnHxfpTtU/3BFQzCQF/2bYVoNbH7zmItbSoMj40JSzmMyX5qDvriA7QdrIIpA+3cdsMpu0nXI8cV0MtKXCPZev+gCEM1S2NHPvWfP/hL+7FSr3+0p5RBEyhEN5JCKYr8XnASMT0xBNyzQGQeI8fjsGD39RMPk7se2bd5ZtTyoFYXftF6y37gx7NeUtJJOTFlAHDZLDuILU3j3+H5oOrD3yWbIztugaAzgnBKJuBLpGfQrS8wO4FZgV+c1IxaLgWVU0tMLEETCos4xMzEIv9cJXQcyagIwigDGwJgOAtHAwAhisQUjy0ORGERiELgG4iakkzo4MYAxcM5hAMi1WWG1yYCJIcMUaBkVRLdGeSU2995TLWzcUAzONJ7J6FBVBYIggMzmFbvdBV44Corg8vjhzC+EJEl8U1kJtgYrhCzgc/vvTwXKSib1paRFVRVORDAJAsw5FuTaJEhWM2SHB3mOAlhkNxwuLzeJsGwqWzf5TFNdKgtY5qHp6ZFf67Y/sAVadCaVY5YACDDb3Oi4NIjLnWMw2QthCBIsVhsUTU9tvXsjeq9+X1d75/KEs4LNOfcdf/+HthMnvwxOD0wmHaXr7ZItn2wuH2SnBzbZAbPJwpPx+VQuzcm7dgRCB57a1uBzUDRL4bfnI0RE0eaXd9W89mpjqHZnUI5Hh2l2dkZZUhOqpi2qSmpOmZ64Tuu9qlz/SEXo6MEHa3wOip46F1n7633eekV8ds8Wxjn37Wl63VVa+ej5oeEZ/82ZBETJjpJ1Rbij2D3Z/1trXUvLsblCK0XfOx0SX2kMsn9dX+d+7Kf6h8o4AIykuffjT8L20LU+w4AZd5VvEPY+XpWqLV327HR7DzXuDnD8r+ovkBehJ8i+y8YAAAAASUVORK5CYII=)"
        },
        warn: {
            "color": "#C09853",
            "background-color": "#FCF8E3",
            "border-color": "#FBEED5",
            "background-image": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAMAAAC6V+0/AAABJlBMVEXr6eb/2oD/wi7/xjr/0mP/ykf/tQD/vBj/3o7/uQ//vyL/twebhgD/4pzX1K3z8e349vK6tHCilCWbiQymn0jGworr6dXQza3HxcKkn1vWvV/5uRfk4dXZ1bD18+/52YebiAmyr5S9mhCzrWq5t6ufjRH54aLs0oS+qD751XqPhAybhwXsujG3sm+Zk0PTwG6Shg+PhhObhwOPgQL4zV2nlyrf27uLfgCPhRHu7OmLgAafkyiWkD3l49ibiAfTs0C+lgCniwD4sgDJxqOilzDWowWFfAH08uebig6qpFHBvH/aw26FfQTQzsvy8OyEfz20r3jAvaKbhgG9q0nc2LbZxXanoUu/u5WSggCtp1anpJKdmFz/zlX/1nGJiYmuq5Dx7+sAAADoPUZSAAAAAXRSTlMAQObYZgAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfdBgUBGhh4aah5AAAAlklEQVQY02NgoBIIE8EUcwn1FkIXM1Tj5dDUQhPU502Mi7XXQxGz5uVIjGOJUUUW81HnYEyMi2HVcUOICQZzMMYmxrEyMylJwgUt5BljWRLjmJm4pI1hYp5SQLGYxDgmLnZOVxuooClIDKgXKMbN5ggV1ACLJcaBxNgcoiGCBiZwdWxOETBDrTyEFey0jYJ4eHjMGWgEAIpRFRCUt08qAAAAAElFTkSuQmCC)"
        }
    }
});;/**
 * Created by stanimirov on 2/13/2015.
 */
var POSITIONING_THE_CURSOR = (function($){
    'use strict';
    var FEATURE_TESTS = {
            addEventListener: !!window.addEventListener
        },
         VAR_HOLDER = {
             getTheInputField: $('#month-input-field'),
             getTheSuggestionWindow:$('#suggestionWindow'),
             getSuggestionAElements: $('.suggestion_window_a'),
             setCursorPositionDelayTime: 300
        },
         FUNC_HOLDER = {
            getCursorPosition: function(elem) {
                var input = elem.get(0);
                if (!input){ return; } // No (input) element found
                if ('selectionStart' in input) {
                    // Standard-compliant browsers
                    return input.selectionStart;
                } else if (document.selection) {
                    // IE
                    input.focus();
                    var sel = document.selection.createRange();
                    var selLen = document.selection.createRange().text.length;
                    sel.moveStart('character', -input.value.length);
                    return sel.text.length - selLen;
                }
            },
            setCursor: function (el,st,end) {
                if(el.setSelectionRange) {
                    el.focus();
                    el.setSelectionRange(st,end);
                }
                else {
                    // IE
                    if(el.createTextRange) {
                        var range = el.createTextRange();
                        range.collapse(true);
                        range.moveEnd(end);
                        range.moveStart(st);
                        range.select();
                    }
                }
            },
            addCursorInPosition: function() {
                if(FEATURE_TESTS.addEventListener){
                    $(VAR_HOLDER.getTheInputField).keyup(function ( event ) {
                        event = event || window.event;
                        /***Down Keyboard Arrow***/
                        if(event.keyCode != 40) {
                            if (!$(VAR_HOLDER.getTheSuggestionWindow).hasClass('hideWindow')) {
                                if ($(VAR_HOLDER.getTheInputField).val()) {
                                    setTimeout(function () {
                                        var positionReturn = FUNC_HOLDER.getCursorPosition($(VAR_HOLDER.getTheInputField));
                                        $(VAR_HOLDER.getTheInputField).focus(function () {
                                            FUNC_HOLDER.setCursor(document.getElementById('month-input-field'), positionReturn, 9);
                                        });
                                    }, VAR_HOLDER.setCursorPositionDelayTime);
                                } else if (!$(VAR_HOLDER.getTheInputField).val()) {
                                    console.log('The suggestion list is empty');
                                }
                            }
                        }
                    });
                }
            }
        };
    FUNC_HOLDER.addCursorInPosition();

    /*********Sending_Data_for_Tests----------->>>*/
    var sendForTesting = {
        inputFieldCapture: VAR_HOLDER.getTheInputField,
        setCursorDelayTimeCapture: VAR_HOLDER.setCursorPositionDelayTime
    };
    return sendForTesting;
    /*<<<---------Sending_Data_for_Tests***********/
})(jQuery);;/**
 * Created by stanimirov on 2/18/2015.
*/
var CUSTOM_EVENT = (function(){

    var msgbox = document.body;
    msgbox.addEventListener("click", SendMessage, false);


// new message: raise newMessage event
    function SendMessage(e) {
        e = e || window.event;
        var event = new CustomEvent("newMessage", {
            detail: {
                inputValue: 'testTEXT',
                importance: 123,
                isDone: true
            },
            bubbles: true,
            cancelable: true
        });
        e.currentTarget.dispatchEvent(event);
    }
})();
;/**
 * Created by stanimirov on 3/30/2015.
 */

var CUSTOM_EVENT = (function($){

    var msgbox = document.body;
    msgbox.addEventListener("submit", sendNotificationParameters, false);


    // new message: raise newMessage event
    function sendNotificationParameters(e) {
        e = e || window.event;
        var event = new CustomEvent("notify", {
            detail: {
                element: $('#month-input-field'),
                message: 'No data to store, please type some month',
                type: 'info'
            },
            options: {
                autoHide: true,
                autoHideDelay: 100
            }
        });
        e.currentTarget.dispatchEvent(event);
    }
})(jQuery);;/**
 * Created by stanimirov on 2/13/2015.
 */
var SHOW_THE_SUGGEST_WINDOW = (function($){
    'use strict';
    var FEATURE_TESTS = {
            addEventListener: !!window.addEventListener
        },
        VARIABLE_CONTAINER = {
            getTheInputField: $('#month-input-field'),
            getTheSuggestionWindow: $('#suggestionWindow'),
            getTheDropDownButton: $('#monthsDropDownButton'),
            getTheSubmitButton: $('#the-submit-button'),
            getItemStoredAttributes: []

        },
        FUNCTION_CONTAINER = {
            showSuggestWindow: function(  ){
                $(VARIABLE_CONTAINER.getTheSuggestionWindow).removeClass('hideWindow');
            },
            hideTheSuggestWindow: function(){
               $(VARIABLE_CONTAINER.getTheSuggestionWindow).addClass('hideWindow');

            }
        };

    /******Show/hide_the_suggestion_window******/

        if(FEATURE_TESTS.addEventListener){
            VARIABLE_CONTAINER.getTheInputField.keydown(function( event ){
                event = event || window.event;
                /***Down Keyboard Arrow***/
                if(event.keyCode != 40){
                    $(VARIABLE_CONTAINER.getTheSuggestionWindow).removeClass( 'hideWindow' );
                    $(VARIABLE_CONTAINER.getTheInputField).focusout(FUNCTION_CONTAINER.hideTheSuggestWindow);
                }
                /***Down Keyboard Arrow***/
                if(event.keyCode == 40){
                    $(VARIABLE_CONTAINER.getTheSuggestionWindow).removeClass(' hideWindow ');
                    $(VARIABLE_CONTAINER.getTheInputField).focusout(FUNCTION_CONTAINER.showSuggestWindow);
                    $( ".suggestion_window_a:first" ).addClass(' aElementsDecoration ').focus();
                }
                /***38- Up; 39- Right keyboard arrows***/
                if( event.keyCode == 38 || event.keyCode == 39  ){
                    FUNCTION_CONTAINER.hideTheSuggestWindow();
                }
            });

            /******Hide suggestion window when the document , month or submit button is clicked******/
            VARIABLE_CONTAINER.getTheDropDownButton.click(FUNCTION_CONTAINER.hideTheSuggestWindow);
            VARIABLE_CONTAINER.getTheSubmitButton.click(FUNCTION_CONTAINER.hideTheSuggestWindow);
            $(document).click(FUNCTION_CONTAINER.hideTheSuggestWindow);
        }

    /*********Sending_Data_for_Tests----------->>>*/

    var sendDataForTests = {
        eListener: FEATURE_TESTS.addEventListener,
        inputFieldCapture: VARIABLE_CONTAINER.getTheInputField,
        suggestionWindowCapture: VARIABLE_CONTAINER.getTheSuggestionWindow
    };
    /*<<<---------Sending_Data_for_Tests***********/
    return sendDataForTests;
})(jQuery);;/**
 * Created by stanimirov on 2/19/2015.
 */

var ADD_MONTH_FORM_THE_DROP_DOWN_LIST = (function($){
    'use strict';

     var FEATURE_TESTS = {
            addEventListener: !!window.addEventListener
    },
        VARIABLE_CONTAINER = {
            getTheInputField: $('#month-input-field'),
            getTheSuggestionWindow: $('#suggestionWindow'),
            getTheForm: $('#months-form'),
            getSuggestionAElements: $('.suggestion_window_a')
        },
        FUNCTION_CONTAINER = {
            captureSuggestionListLiElements: function( callback ){
                if(FEATURE_TESTS.addEventListener){
                    var data  ;
                    $(VARIABLE_CONTAINER.getTheInputField).keyup(function( event ){
                        event = event || window.event;
                        /***Down Keyboard Arrow***/
                        if(event.keyCode != 40) {
                            if (!$(VARIABLE_CONTAINER.getTheSuggestionWindow).hasClass('hideWindow')) {
                                setTimeout(function () {
                                    var aElements = $('.suggestion_window_a');
                                    data = aElements;
                                    if (callback && typeof(callback) === "function") {
                                        // execute the callback, passing parameters as necessary
                                        callback.apply( data );
                                    }
                                }, 400);
                            }
                        }
                    });
                }
            },
            showSuggestWindow: function(  ){
                VARIABLE_CONTAINER.getTheSuggestionWindow.removeClass('hideWindow');
            },
            hideTheSuggestWindow: function(){
                VARIABLE_CONTAINER.getTheSuggestionWindow.addClass('hideWindow');

            }
        };

 FUNCTION_CONTAINER.captureSuggestionListLiElements(function( data ) {
     data = data || {};
        if(FEATURE_TESTS.addEventListener){
            var getAElements = $('.suggestion_window_a');
            /*******Makes 'a' elements clickable*******/
            $(getAElements).mouseover(function(){
                VARIABLE_CONTAINER.getTheInputField.focusout(FUNCTION_CONTAINER.showSuggestWindow);
            });

            $(getAElements).click(function(){
                $(VARIABLE_CONTAINER.getTheInputField.val($(this).text()));
                /****comes from focusTextToEnd.js module****/
                VARIABLE_CONTAINER.getTheInputField.focusTextToEnd();
                FUNCTION_CONTAINER.hideTheSuggestWindow();
            });

            getAElements.keydown(function( event ){
                /***Down Keyboard Arrow***/
                if( event.keyCode == 40){
                    if(this.parentNode.nextSibling.childNodes !== null){
                        $(this.parentNode.nextSibling.childNodes).addClass('aElementsDecoration').focus();
                        $(this).removeClass('aElementsDecoration');
                    }
                }
                /***Up Keyboard Arrow***/
                if(event.keyCode == 38){
                        if(this.parentNode.previousSibling.childNodes !== null){
                            $(this.parentNode.previousSibling.childNodes).addClass('aElementsDecoration').focus();
                            $(this).removeClass('aElementsDecoration');
                        }
                     }
                /***Keyboard Escape Key***/
                if(event.keyCode == 27){
                    VARIABLE_CONTAINER.getTheInputField.val('').focus();
                    VARIABLE_CONTAINER.getTheSuggestionWindow.addClass('hideWindow');
                    }
                });
        }
});

    /*Return For Tests*/
    var sendForTesting = {
        eListener: FEATURE_TESTS.addEventListener,
        inputFieldCapture: VARIABLE_CONTAINER.getTheInputField,
        suggestionWindowCapture: VARIABLE_CONTAINER.getTheSuggestionWindow,
        formCapture: VARIABLE_CONTAINER.getTheForm
    };

    return sendForTesting;
})(jQuery);