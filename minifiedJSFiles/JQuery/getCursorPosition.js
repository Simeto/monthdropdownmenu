/* MonthDropDownMenu 2015-01-30 */
!function(a) {
    a.fn.getCursorPosition = function() {
        var a = this.get(0);
        if (a) {
            if ("selectionStart" in a) return a.selectionStart;
            if (document.selection) {
                a.focus();
                var b = document.selection.createRange(), c = document.selection.createRange().text.length;
                return b.moveStart("character", -a.value.length), b.text.length - c;
            }
        }
    };
}(jQuery);