/* MonthDropDownMenu 2015-02-12 */
var POSITIONING_THE_CURSOR = function(a) {
    "use strict";
    var b = {
        getTheInputField: a("#month-input-field"),
        setCursorPositionDelayTime: 200
    }, c = {
        getCursorPosition: function(a) {
            var b = a.get(0);
            if (b) {
                if ("selectionStart" in b) return b.selectionStart;
                if (document.selection) {
                    b.focus();
                    var c = document.selection.createRange(), d = document.selection.createRange().text.length;
                    return c.moveStart("character", -b.value.length), c.text.length - d;
                }
            }
        },
        setCursor: function(a, b, c) {
            if (a.setSelectionRange) a.focus(), a.setSelectionRange(b, c); else if (a.createTextRange) {
                var d = a.createTextRange();
                d.collapse(!0), d.moveEnd("character", c), d.moveStart("character", b), d.select();
            }
        },
        addCursorInPosition: function() {
            a(b.getTheInputField).keyup(function() {
                a(b.getTheSuggestionWindow).hasClass("hideWindow") || (a(b.getTheInputField).val() ? setTimeout(function() {
                    var d = c.getCursorPosition(a(b.getTheInputField));
                    a(b.getTheInputField).focus(function() {
                        c.setCursor(document.getElementById("month-input-field"), d, d);
                    });
                }, b.setCursorPositionDelayTime) : a(b.getTheInputField).val() || console.log("The suggestion list is empty"));
            });
        }
    };
    c.addCursorInPosition();
}(jQuery);