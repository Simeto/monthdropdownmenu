/* MonthDropDownMenu 2015-02-12 */
var SHOW_THE_SUGGEST_WINDOW = function(a) {
    "use strict";
    var b = {
        addEventListener: !!window.addEventListener
    }, c = {
        getTheInputField: a("#month-input-field"),
        getTheSuggestionWindow: a("#suggestionWindow")
    }, d = {
        showSuggestWindow: function() {
            a("#suggestionWindow").removeClass("hideWindow");
        },
        hideTheSuggestWindow: function() {
            a("#suggestionWindow").addClass("hideWindow");
        }
    };
    b.addEventListener && c.getTheInputField.keypress(d.showSuggestWindow).focusout(d.hideTheSuggestWindow);
    var e = {
        eListener: b.addEventListener,
        inputFieldCapture: c.getTheInputField,
        suggestionWindowCapture: c.getTheSuggestionWindow
    };
    return e;
}(jQuery);