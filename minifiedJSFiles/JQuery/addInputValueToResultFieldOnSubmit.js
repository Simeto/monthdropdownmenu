/* MonthDropDownMenu 2015-02-12 */
var ADD_INPUT_VALUE_TO_RESULT_FIELD_ON_SUBMIT = function(a) {
    "use strict";
    var b = {
        addEventListener: !!window.addEventListener
    }, c = {
        getTheForm: a("#months-form"),
        getTheInputField: a("#month-input-field"),
        getTheResultsField: a("#results-field")
    };
    b.addEventListener && a(c.getTheForm).submit(function(b) {
        a(c.getTheInputField).val() ? (b.preventDefault(), c.getTheResultsField.html(a(c.getTheInputField).val()), 
        a(c.getTheInputField).val("")) : (b.preventDefault(), console.log("no data to store"));
    });
    var d = {
        eListener: b.addEventListener,
        formCapture: c.getTheForm,
        inputFieldCapture: c.getTheInputField,
        resultFieldCapture: c.getTheResultsField
    };
    return d;
}(jQuery);