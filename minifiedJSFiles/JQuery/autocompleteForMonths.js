/* MonthDropDownMenu 2015-02-12 */
var AUTOCOMPLETE_FOR_MONTHS = function(a) {
    "use strict";
    var b = {
        addEventListener: !!window.addEventListener
    }, c = {
        getTheInputField: a("#month-input-field"),
        getTheSuggestionWindow: a("#suggestionWindow"),
        getTheAutocompletePanel: a("#autocomplete-panel"),
        suggestionDelayTime: 400
    }, d = {
        monthsArray: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]
    }, e = {
        arrayToLowerCase: a.map(d.monthsArray, function(a) {
            return a.toLowerCase().split();
        }),
        monthsAutocompleteIteration: function() {
            b.addEventListener && a(c.getTheInputField).keyup(function() {
                if (c.getTheAutocompletePanel.html(""), a(c.getTheInputField).val()) {
                    var b, d, f = a(c.getTheInputField).val().toLowerCase(), g = new RegExp("\\b(" + f + ")");
                    for (b = 0, d = e.arrayToLowerCase.length; d > b; b += 1) g.exec(e.arrayToLowerCase[b]) && c.getTheAutocompletePanel.append('<li class="suggestion_window_li">' + e.arrayToLowerCase[b] + "</li>");
                }
            });
        },
        suggestionsTimeout: function() {
            setTimeout(function() {
                e.monthsAutocompleteIteration();
            }, c.suggestionDelayTime);
        }
    };
    e.suggestionsTimeout();
    var f = {
        eListener: b.addEventListener,
        inputFieldCapture: c.getTheInputField,
        suggestionWindowCapture: c.getTheSuggestionWindow,
        autocompletePannelCapture: c.getTheAutocompletePanel,
        suggestionDelayTimeCapture: c.suggestionDelayTime,
        monthsArrayCapture: d.monthsArray,
        arrayToLowercaseCapture: e.arrayToLowerCase,
        monthsAutocompleteIterationCapture: e.monthsAutocompleteIteration,
        suggestionsTimeoutCapture: e.suggestionsTimeout
    };
    return f;
}(jQuery);