/* MonthDropDownMenu 2015-02-12 */
var ADD_MONTH_FROM_DROP_DOWN_LIST = function(a) {
    "use strict";
    var b = {
        addEventListener: !!window.addEventListener
    }, c = {
        getAElements: a("li"),
        getTheInputField: a("#month-input-field")
    };
    b.addEventListener && a(c.getAElements).click(function() {
        c.getTheInputField.val(a(this).text()), c.getTheInputField.focus();
    });
    var d = {
        eListener: b.addEventListener,
        liCapture: c.getAElements,
        inputFieldCapture: c.getTheInputField
    };
    return d;
}(jQuery);