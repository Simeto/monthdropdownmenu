/* MonthDropDownMenu 2015-02-12 */
var ADD_FIRST_MONTH_FROM_THE_SUGGESTION_LIST = function(a) {
    "use strict";
    var b = {
        getTheSuggestionWindow: a("#suggestionWindow"),
        getTheInputField: a("input:text"),
        addFirstMonthDelayTime: 250,
        getTheForm: a("#months-form")
    }, c = {
        addCursorInPosition: function() {
            a(b.getTheInputField).keyup(function() {
                a(b.getTheSuggestionWindow).hasClass("hideWindow") || (a(b.getTheInputField).val() ? setTimeout(function() {
                    a(b.getTheInputField).val(a(".inner:nth-child(1)").text()).focus();
                }, b.addFirstMonthDelayTime) : a(b.getTheInputField).val() || console.log("The suggestion list is empty"));
            });
        }
    };
    c.addCursorInPosition();
    var d = {
        suggestionListCapture: b.getTheSuggestionWindow,
        inputFieldCapture: b.getTheInputField,
        addFirstMonthDelayTimeCapture: b.addFirstMonthDelayTime,
        getTheFormCapture: b.getTheForm
    };
    return d;
}(jQuery);